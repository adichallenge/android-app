package com.example.challenge

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.challenge.data.Product

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ProductDetailFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val product = arguments?.getParcelable<Product>("product")
        val model: ProductReviewsViewModel by viewModels()
        val recyclerView = view.findViewById<RecyclerView>(R.id.reviewsRecycler)

        product?.let { it ->
            view.findViewById<TextView>(R.id.productName).text = it.name
            view.findViewById<TextView>(R.id.productPrice).text = "${it.price} ${it.currency}"
            val productImage = view.findViewById<ImageView>(R.id.productImage)
            Glide.with(requireContext()).load(it.imgUrl).into(productImage)
            model.getReviewsForProduct(it.id).observe(viewLifecycleOwner) {list ->
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.adapter = SimpleRVAdapter(list.map { review -> "${review.text} ${review.rating}"  }.toTypedArray())
            }
        }

        view.findViewById<Button>(R.id.addReview).setOnClickListener {
            product?.let {
                val action =
                    ProductDetailFragmentDirections.actionSecondFragmentToCreateReviewFragment(it)
                findNavController().navigate(action)
            }
        }
    }
}