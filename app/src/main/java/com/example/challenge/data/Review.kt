package com.example.challenge.data

data class Review(val productId : String, val locale: String, val rating: Int, val text : String)