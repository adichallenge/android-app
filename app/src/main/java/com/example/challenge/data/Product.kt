package com.example.challenge.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Product(val id : String, val name : String, val description : String, val currency : String, val imgUrl : String, val price : Double) : Parcelable