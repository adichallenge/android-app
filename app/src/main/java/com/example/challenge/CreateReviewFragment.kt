package com.example.challenge

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.challenge.data.Product
import com.example.challenge.data.Review

class CreateReviewFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.create_review_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model: ProductReviewsViewModel by viewModels()

        val product = arguments?.getParcelable<Product>("product")

        val spinner: Spinner = view.findViewById(R.id.reviewNumber)
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.reviews,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }


        view.findViewById<Button>(R.id.saveReview).setOnClickListener {
            product?.let {
                val text = view.findViewById<EditText>(R.id.reviewDetails).text.toString()
                val review = Review(locale = "en-US",
                    rating = spinner.selectedItem.toString().toInt(),
                    text = text,
                    productId = product.id)
                model.postReview(it.id, review)
                findNavController().popBackStack()
            }
        }
    }
}

