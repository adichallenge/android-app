package com.example.challenge

import com.example.challenge.data.Product
import com.example.challenge.network.ProductService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ProductRepository {
    private val service : ProductService = ServiceBuilder<ProductService>().build(ProductService::class.java)

    fun getProducts() : Call<List<Product>> {
        return service.getProducts()
    }
}