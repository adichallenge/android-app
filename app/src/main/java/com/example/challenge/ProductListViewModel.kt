package com.example.challenge

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challenge.data.Product
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductListViewModel : ViewModel() {
    private val repository = ProductRepository()
    private val products = MutableLiveData<List<Product>>()

    fun getProducts(): LiveData<List<Product>> {
        repository.getProducts().enqueue(object : Callback<List<Product>> {
                override fun onResponse(call: Call<List<Product>>, response: Response<List<Product>>) {
                    if (response.isSuccessful) {
                        products.postValue(response.body())
                    }
                }
                override fun onFailure(call: Call<List<Product>>, t: Throwable) {
                    products.postValue(emptyList())
                }
            })
        return products
    }

}