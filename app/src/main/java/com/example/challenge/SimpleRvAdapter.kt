package com.example.challenge

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class SimpleRVAdapter(private val dataSource: Array<String>) :
    RecyclerView.Adapter<SimpleRVAdapter.SimpleViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SimpleViewHolder {
        // create a new view
        val view: View = TextView(parent.context)
        return SimpleViewHolder(view)
    }

    class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView: TextView

        init {
            textView = itemView as TextView
        }
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.textView.text = dataSource[position]
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }
}