package com.example.challenge

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.challenge.data.Product

class ProductAdapter (private var dataSet: List<Product>, private var action : (Product) -> Unit) :
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val productName: TextView = view.findViewById(R.id.productName)
        val productPrice: TextView = view.findViewById(R.id.productPrice)
        val image : ImageView = view.findViewById(R.id.productImage)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.product_cell, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.productName.text = dataSet[position].name
        viewHolder.productPrice.text = "${dataSet[position].price} ${dataSet[position].currency}"
        viewHolder.itemView.setOnClickListener {
            action(dataSet[position])
        }
        Glide.with(viewHolder.productName.context).load(dataSet[position].imgUrl).into(viewHolder.image);


    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

    fun updateItems(products: List<Product>?) {
        dataSet = products?: emptyList()
        notifyDataSetChanged()
    }

}