package com.example.challenge

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challenge.data.Review
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductReviewsViewModel : ViewModel() {
    private val repository = ReviewsRepository()
    private val reviews = MutableLiveData<List<Review>>()

    fun getReviewsForProduct(productId : String): LiveData<List<Review>> {
        repository.getReviews(productId).enqueue(object : Callback<List<Review>> {
            override fun onResponse(call: Call<List<Review>>, response: Response<List<Review>>) {
                if (response.isSuccessful) {
                    reviews.postValue(response.body())
                }
            }
            override fun onFailure(call: Call<List<Review>>, t: Throwable) {
                reviews.postValue(emptyList())
            }
        })
        return reviews
    }

    fun postReview(productId: String, review : Review) {
        repository.postReview(productId, review).enqueue(object : Callback<Review> {
            override fun onResponse(call: Call<Review>, response: Response<Review>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        val list = reviews.value?.toMutableList()
                        list?.add(it)
                        reviews.postValue(list)
                    }
                }
            }
            override fun onFailure(call: Call<Review>, t: Throwable) {

            }
        })
    }

}
