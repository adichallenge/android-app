package com.example.challenge.network

import com.example.challenge.data.Review
import retrofit2.Call
import retrofit2.http.*

interface ReviewsService {
    @GET("reviews/{productId}")
    @Headers("Accept-Language: en-US")
    fun getReviews(@Path("productId") productId : String): Call<List<Review>>

    @POST("reviews/{productId}")
    @Headers("Accept-Language: en-US")
    fun postReview(@Path("productId") productId : String, @Body review: Review): Call<Review>

}