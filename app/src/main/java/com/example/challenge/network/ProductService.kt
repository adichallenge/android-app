package com.example.challenge.network

import com.example.challenge.data.Product
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers

interface ProductService {
    @GET("product")
    @Headers("Accept-Language: en-US")
    fun getProducts(): Call<List<Product>>
}