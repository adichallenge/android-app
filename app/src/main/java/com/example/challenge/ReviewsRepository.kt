package com.example.challenge

import com.example.challenge.data.Review
import com.example.challenge.network.ReviewsService
import retrofit2.Call

class ReviewsRepository {
    private var service : ReviewsService = ServiceBuilder<ReviewsService>().build(ReviewsService::class.java)

    fun getReviews(productId : String) : Call<List<Review>> {
        return service.getReviews(productId)
    }

    fun postReview(productId: String, review: Review) : Call<Review> {
        return service.postReview(productId, review)
    }
}